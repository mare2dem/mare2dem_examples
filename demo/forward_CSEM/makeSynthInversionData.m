
clear stNoise
inputFile  = 'demo.0.resp';
outputFile = 'demo_csem_synth.emdata';
stNoise.csem.relNoise = 0.05; % fraction of noise to add. 0.05 means 5%
stNoise.csem.minAmpE = 1d-15; % electric field responses below this amplitude 
                              % are omitted from the output data file.
                         

m2d_makeSyntheticData(inputFile,outputFile,stNoise)

plotMARE2DEM_CSEM(outputFile); % plot check