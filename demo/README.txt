
This folder contains demonstration examples of MARE2DEM forward and inverse
modeling of MT and CSEM data.

The file topo.txt contains the position and depth of the topography used
in the models.

Forward Modeling 
----------------

Folders ./forward_MT and ./forward_CSEM are examples for MT and CSEM
forward modeling for the demo model. See the MATLAB scripts
makeForwardData.m in each of those folders for examples showing how to
make the data files specifying the frequencies, receivers, transmitters
and data types to forward model.

Run MARE2DEM using (for example) 8 mpi processes:

mpirun -n 8 MARE2DEM demo.0.resistivity

When completed, MARE2DEM outputs the response file demo.0.resp, which
can be viewed in plotMARE2DEM_MT.m or plotMARE2DEM_CSEM.m, depending on
the particular data type.

Synthetic Inversion 
-------------------

The demo.0.resp files in ./forward_MT and ./forward_CSEM an be turned
into synthetic data files by adding random noise to the forward
responses. These can then be inverted to get an idea of how well the
particular data are able to recover the forward model structure. The
routine makeSynthInversionData.m shows how to make the synthetic data
files.  Create a new folder for the inversion, copy the synthetic data
file to that folder, and then use Mamba2D.m to create an inversion grid
(ideally in another folder). Don't forget to specify the synthetic data
file in the inversion panel in Mamba2D. Write the inversion grid to file
in Mamba2D.m and you'll be ready to run a MARE2DEM inversion

The various ./inversion_* folders are pre-run example inversions for the 
various data types.

Run a MARE2DEM inversion using the same command as above:

mpirun -n 8 MARE2DEM  demo.0.resistivity

MARE2DEM will output three files for each inversion iteration
demo.<iteration#>.resistivity, demo.<iteration#>.resp and
demo.<iteration#>.sensitivity.


