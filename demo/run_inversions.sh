#!/usr/bin/env -S -i zsh  # ensure clean environment.
#
# Batch script for running the demo examples. 
# 
#  I use this for running all the demo forward and inversion examples
#  without requiring manual intervention. 
#  
#  New users learning to use the codes:

#  You will not want to use this script and instead should run MARE2DEM
#  and the MATLAB codes manually. However, for more routine production
#  usage, this script is a nice example showing how you can run both
#  MARE2DEM and some of the MATLAB routines from a shell script. You
#  will need to customize the m2d and mtlb commands for your platform.


m2d="mpirun -n 29 -oversubscribe MARE2DEM demo.0.resistivity"
mtlb="/Applications/MATLAB_R2022a.app/bin/matlab"

# Run forward_MT, generate synthetic noisy data and distribute to inversion folders: 
cd ./forward_MT
eval "$m2d"
eval "$mtlb -batch 'makeSynthInversionData' "
cp  demo_mt_synth.emdata ../inversion_MT
cp  demo_mt_synth.emdata ../inversion_MT_static
cp  demo_mt_synth.emdata ../inversion_MT_cut
cd ..


# Run forward_CSEM, generate synthetic noisy data and distribute to inversion folders: 
cd ./forward_CSEM
eval "$m2d"
eval "$mtlb -batch 'makeSynthInversionData' "
cp  demo_csem_synth.emdata ../inversion_CSEM
cd ..


cd ./inversion_CSEM_MT
eval "$mtlb -batch 'make_joint_data_file' "
cd ..
 
# Now run all inversions:
cd ./inversion_MT
eval "$m2d"
cd ..

cd ./inversion_MT_static
eval "$mtlb -batch 'makeSyntheticStaticShift' "
eval "$m2d"
cd ..

cd ./inversion_MT_cut
eval "$m2d"
cd ..

cd ./inversion_CSEM
eval "$m2d"
cd ..

cd ./inversion_CSEM_MT
eval "$m2d"
cd ..