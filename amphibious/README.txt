The examples in the subdirectories here are for amphibious MT modeling.

./mt_forward/ has an example amphibious MT profile over a 2D model with significant resistivity variations.
The marine MT responses show classic coastline effects including cusping in the TE mode and a depressed
TM mode response. 

./mt_1ohmm_halfspace_forward/ has the same profile but now for a 1 ohm-m halfspace under the topography.
Note how the coast effects in the responses are much smaller for this more conductive model.

./mt_forward_fields/ uses a 2D array of receivers in y,z to show the coast-effect MT field behaviour at 60 s period.
See the README.txt file in that subdirectory for more info.


