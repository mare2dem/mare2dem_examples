
sFile = 'simple.emdata';   % output file name

topo = [];

st = [];

st.stCSEM.frequencies  = [400];


st.stCSEM.rx_z         = -5+[50:5:290]'; 
st.stCSEM.rx_y         = -100*ones(size(st.stCSEM.rx_z));
 
st.stCSEM.tx_z         = st.stCSEM.rx_z - 5;
st.stCSEM.tx_y         = 100*ones(size(st.stCSEM.tx_z));
st.stCSEM.tx_dip       = 90;  % vertical Tx
st.stCSEM.tx_azimuth   = 0;  
st.stCSEM.tx_type      = 'bdipole';


st.stCSEM.lBz          = true;

% make data:
m2d_makeDataFile(sFile,topo,'forward',st)

% plot .resistivity file with MT receivers on top to confirm receivers are
% located at the correct positions and depth:
plotMARE2DEM('newest'); % 'newest' plots the most recent .resistivity file
 

