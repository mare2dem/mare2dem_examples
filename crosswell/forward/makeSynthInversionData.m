 
inputFile   = 'simple.0.resp';
outputFile  = 'simple_synth.emdata';
stNoise.csem.relNoise        = 0.01;  % this means 1 percent random noise.

m2d_makeSyntheticData(inputFile,outputFile,stNoise)

plotMARE2DEM_CSEM(outputFile)
 
% then move outputFile into the inversion folder and run MARE2DEM to invert
% it